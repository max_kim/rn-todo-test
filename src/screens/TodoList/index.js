// @flow
import React, { memo } from 'react';
import styled from 'styled-components';

import type { Todo } from '../../types/todo';

type Props = {
  todosList: Todo[],
};

export const TodoList = memo<Props>(({ todosList }: Props) => {
  return (
    <Container>
      <StyledList
        data={todosList}
        renderItem={({ item }) => (
          <TodoItem>
            <Text>{item.title}</Text>
          </TodoItem>
        )}
      />
    </Container>
  );
});

const Container = styled.SafeAreaView`
  height: 100%;
  width: 100%;
`;

const StyledList = styled.FlatList`
  height: 100%;
  width: 100%;
`;

const TodoItem = styled.View`
  flex-direction: row;
  align-items: center;
  height: 32px;
  width: 100%;
  padding: 0 16px;
`;

const Text = styled.Text`
  font-size: 13px;
`;
