// @flow
import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

export const Loader = () => {
  return (
    <View style={StyleSheet.absoluteFill}>
      <ActivityIndicator size={'large'} />
    </View>
  );
};
