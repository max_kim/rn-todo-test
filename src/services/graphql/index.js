import { persistCache } from 'apollo-cache-persist';
import {
  ApolloClient,
  ApolloLink,
  createHttpLink,
  InMemoryCache,
} from '@apollo/client';
import AsyncStorage from '@react-native-community/async-storage';

import { typeDefs } from './schema';
import { API_URL } from '../../constants/api';

export const getApolloClient = async (queueLink) => {
  const cache = new InMemoryCache({});

  const httpLink = createHttpLink({
    uri: API_URL,
  });

  const link = ApolloLink.from([queueLink, httpLink]);

  await persistCache({
    cache,
    storage: AsyncStorage,
  });

  return new ApolloClient({ cache, link, typeDefs });
};
