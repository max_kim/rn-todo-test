import { gql } from '@apollo/client';

export const typeDefs = gql`
  type Query {
    getAllTodos: [Todo]!
    getTodo(id: ID!): Todo!
  }

  type Todo {
    id: ID
    title: String!
    completed: Boolean!
  }

  input TodoInput {
    title: String!
  }

  type Mutation {
    addTodo(todo: TodoInput): Todo!
    updateTodo(id: ID): [Todo]!
    removeTodo(id: ID): [Todo]!
  }
`;
