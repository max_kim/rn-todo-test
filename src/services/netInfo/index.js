// @flow
import React, { useEffect } from 'react';
import NetInfo from '@react-native-community/netinfo';
import QueueLink from 'apollo-link-queue';

type Props = {
  queueLink: typeof QueueLink,
};

export const NetInfoService = React.memo<Props>(({ queueLink }: Props) => {
  useEffect(() => {
    return NetInfo.addEventListener((state) => {
      if (state.isConnected) {
        queueLink.open();
      } else {
        queueLink.close();
      }
    });
  }, [queueLink]);

  return null;
});
