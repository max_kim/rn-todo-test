// @flow
import React, { useState } from 'react';
import { ApolloProvider, useQuery } from '@apollo/client';
import QueueLink from 'apollo-link-queue';

import { TodoList } from './src/screens/TodoList';
import { Loader } from './src/presentations/common/Loader';
import { getApolloClient } from './src/services/graphql';
import { NetInfoService } from './src/services/netInfo';
import { GET_ALL_TODOS } from './src/services/graphql/todo';

const queueLink = new QueueLink();

export const App: () => React$Node = () => {
  const [client] = useState(async () => await getApolloClient(queueLink));

  const { data, error, loading } = useQuery(GET_ALL_TODOS);

  console.log('#########');
  console.log('data', data);
  console.log('error', error);
  console.log('loading', loading);
  console.log('#########');

  return client ? (
    <>
      <ApolloProvider client={client}>
        {loading ? <Loader /> : <TodoList todosList={data} />}
      </ApolloProvider>
      <NetInfoService queueLink={queueLink} />
    </>
  ) : (
    <Loader />
  );
};
