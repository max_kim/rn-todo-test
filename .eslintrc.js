module.exports = {
  root: true,
  extends: [
    '@react-native-community',
    "prettier",
    "prettier/babel",
    "prettier/flowtype",
    "prettier/react",
  ],
};
